#Unidades Formativas

* Sintaxi del llenguatge. Objectes predefinits del llenguatge.
* Esdeveniments. Manegament de formularis. Model d'objectes del document.
* Estructures definides pel programador.
* Comunicació asíncrona client-servidor.
